/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oopdesignpatterns.patterns;

import oopdesignpatterns.Pattern;
import oopdesignpatterns.StandardPatternOutput;
import oopdesignpatterns.PatternOutput;


/**
 *
 * @author salvix
 */
public class Test implements Pattern {
    
    public PatternOutput perform(){
        StandardPatternOutput po = new StandardPatternOutput();
        po.setMessage("A test pattern!");
        return po;
    }
}
