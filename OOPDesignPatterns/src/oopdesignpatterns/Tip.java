/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oopdesignpatterns;

/**
 *
 * @author salvix
 */
public class Tip {
    private String identifier;
    private String message;
    
    public Tip(String i, String m){
        this.identifier = i;
        this.message = m;
    }
    
    public String getMessage(){
        return this.message;
    }
    
    public String getIdentifier(){
        return this.identifier;
    }
}
