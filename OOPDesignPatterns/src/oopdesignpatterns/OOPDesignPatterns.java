/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oopdesignpatterns;

import java.util.Map;
import java.util.HashMap;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import oopdesignpatterns.Tip;

/**
 *
 * @author salvix
 */
public class OOPDesignPatterns implements SimpleNotifiable, Runnable {
	
    private mainGUI frame;
    private Map map;
    private static String tips = "tips.dat";
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        log("Preparing to launch...");

        /* Create and display the form */
        OOPDesignPatterns od = new OOPDesignPatterns();
        od.run();
        log("Launched!");

    }

    
    
    private void executePattern(String patternName){
        try{
            Class c = Class.forName("oopdesignpatterns.patterns."+patternName);
            Object pattern;
            try {
                pattern = c.newInstance();
                Pattern p = (Pattern) pattern;
                PatternOutput po = p.perform();
                String output = po.toString();
                display(output);    
            } 
            catch (InstantiationException ex) {
                log(OOPDesignPatterns.class.getName());
            } 
            catch (IllegalAccessException ex) {
                log(OOPDesignPatterns.class.getName());
            }

        }
        catch (ClassNotFoundException e){
            String m = "Caught ClassNotFoundException "+e.getMessage();
            log(m);
            display("Pattern "+patternName+" not found!");
        }
    }
    
    
    private void executeTip(String tipName){
        log(tipName);
        Object v = map.get(tipName);
        if(v != null){
            Tip t = (Tip) v;
            display(t.getMessage());
        }
        else{
            display("Not found!");
        }
    }
    
    private void execute(String command){
        int split = command.indexOf('.');
        if(split > 0){
            String type = command.substring(0, split);
            String data = command.substring(split+1);
            switch(type){
                case "tip":
                    this.executeTip(data);
                break;
                case "pattern":
                    this.executePattern(data);
                break;
                default:
                    log("Mysterious type choosen");
                    display("Nothing to do!");
                break;
            }            
        }
        else{
            log("Mysterious type choosen");
            display("Nothing to do!");
        }
    }
    
    public void message(Object o){
        String className = (String) o;
        this.execute(className);
    }

    public static void log(String s){
        System.out.println(s);
    }
    
    public void display(String s){
        frame.writeText(s);
    }
    
    
    private void initFrame(){
        frame = new mainGUI();
        frame.notifyTo(this);
        frame.setVisible(true);        
    }
    
    private void initMap(){
        map = new HashMap<String, Tip>();
        try {
            InputStream is = getClass().getResourceAsStream(tips);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            String id;
            String message;
            while ((line = br.readLine()) != null){
                int split = line.indexOf('|');
                if(split > 0){
                    id = line.substring(0, split);
                    message = line.substring(split+1);
                    Tip tip = new Tip(id, message);
                    map.put(id, tip);
                }
            }
        } 
        catch (IOException ex) {
            log("Could not find "+tips);
        }
    }
    
    public void run() {
        initFrame();
        initMap(); 
    }
    

    

}
