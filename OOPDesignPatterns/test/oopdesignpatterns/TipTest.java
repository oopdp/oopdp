/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oopdesignpatterns;

import org.junit.Test;
import static org.junit.Assert.*;

import oopdesignpatterns.Tip;
import org.junit.Assert;
/**
 *
 * @author salvix
 */
public class TipTest {
    
    public TipTest() {
    }

    @Test
    public void testTip() {
        String i = "i";
        String m = "m";
        Tip t = new Tip(i, m);
        Assert.assertEquals(t.getMessage(), m);
        Assert.assertEquals(t.getIdentifier(), i);
    }
    
}
