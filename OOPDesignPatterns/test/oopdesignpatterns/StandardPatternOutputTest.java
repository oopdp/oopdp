/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oopdesignpatterns;

import junit.framework.Assert;
import org.junit.Test;
import oopdesignpatterns.StandardPatternOutput;
import static org.junit.Assert.*;

/**
 *
 * @author salvix
 */
public class StandardPatternOutputTest {
    
    public StandardPatternOutputTest() {
    }

    @Test
    public void testSomeMethod() {
        Assert.assertEquals(1, 1);
    }
    
    @Test
    public void testSetMessage() {
        String m = "This is the message";
        StandardPatternOutput so = new StandardPatternOutput();
        so.setMessage(m);
        String got = so.getMessage();
        assertEquals(got, m);
    }
    
    @Test
    public void testToString() {
        String m = "This is the message";
        StandardPatternOutput so = new StandardPatternOutput();
        so.setMessage(m);
        assertEquals(so+"", m);
    }
    
    
}
